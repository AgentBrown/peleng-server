import rsa
from crypto import PublicKey
import os.path
import re
import base64
import sqlite3
import threading
from bottle import route, request, run, get, post, response, redirect
from point import Point
from hashlib import md5


points = []
conn = None
teams = []

def init():
    (publicKey, privateKey) = rsa.newkeys(1024)
    encPublicKey = publicKey.save_pkcs1('PEM')
    encPrivateKey = privateKey.save_pkcs1('PEM')
    publicFile = open(os.path.join('mykeys', 'public.pem'), 'wb')
    privateFile = open(os.path.join('mykeys', 'private.pem'), 'wb')
    publicFile.write(encPublicKey)
    privateFile.write(encPrivateKey)
    publicFile.close()
    privateFile.close()

def loadFromDB(cursor):
    cursor.execute('SELECT `id`, `lat`, `long`, `eps`, `belongs`, `weight` FROM Points')
    for r in cursor.fetchall():
        points.append(Point(r[1], r[2], r[3]))
        points[-1].id = r[0]
        points[-1].belongs = r[4]
        points[-1].weight = r[5]

def getFullTeams(cursor):
    global teams
    result = dict()
    for team in teams:
        cursor.execute('SELECT `name` FROM `Users` WHERE `team`=?', (team,))
        result[team] = list(map(lambda t: t[0], cursor.fetchall()))
    return result

def addPlayer(cursor, nick, team):
    cursor.execute('INSERT INTO Users VALUES (NULL, ?, ?)', (nick, team))

def playerTeam(cursor, nick):
    cursor.execute('SELECT `team` FROM `Users` WHERE `name`=?', (nick,))
    result = cursor.fetchone()
    return result[0] if result is not None else None

def getPointsByTeam(cursor, team):
    cursor.execute('SELECT `lat`, `long`, `weight`, `eps` FROM Points WHERE `belongs`=?', (team,))
    result = []
    for row in cursor.fetchall():
        result.append(Point(row[0], row[1], row[3]))
        result[-1].weight = row[2]
        result[-1].belongs = team
    return result

def isUserExist(nick):
    return os.path.isfile(os.path.join('userkeys', nick + '.pem'))

def getUserKey(nick):
    keyFile = open(os.path.join('userkeys', nick + '.pem'), 'rb')
    key = rsa.PublicKey.load_pkcs1(keyFile.read())
    keyFile.close()
    return key

def getSelfPrivateKey():
    keyFile = open(os.path.join('mykeys', 'private.pem'))
    key = rsa.PrivateKey.load_pkcs1(keyFile.read().encode('utf-8'))
    keyFile.close()
    return key

def getSelfPublicKey():
    keyFile = open(os.path.join('mykeys', 'public.pem'))
    key = rsa.PublicKey.load_pkcs1(keyFile.read().encode('ascii'))
    keyFile.close()
    return key

def decryptMessage(nick, message, signature):
    message = base64.b64decode(message)
    print(signature)
    signature = base64.b64decode(signature)
    userKey = getUserKey(nick)
    selfKey = getSelfPrivateKey()
    rsa.verify(message, signature, userKey)
    return rsa.decrypt(message, selfKey)

def increaseAllPoints():
    global points, conn
    for point in points:
        point.weight *= 1.5
        point.exportToDB(conn.cursor())
    conn.commit()
    threading.Timer(6 * 60 * 60, increaseAllPoints) # 6 hours (6 hrs * 60 min in hour * 60 seconds in minute)
        
@post('/newclient')
def newClient():
    try:
        request.json
    except:
        response.status = 400
        return 'Invalid JSON!'
    if type(request.json) != type(dict()):
        response.status = 400
        return 'JSON outer type must be object!'
    if 'nick' not in request.json:
        response.status = 400
        return 'Nick is not present!'
    if 'ukey' not in request.json:
        response.status = 400
        return 'Public key is not present!'
    userLogin = str(request.json['nick'])
    publicKey = request.json['ukey'].encode('utf-8')
    if not re.match('^\w+', userLogin):
        print('Incorrect user!')
        response.status = 400
        return 'Incorrect nickname!'
    if isUserExist(userLogin):
        print('Exists!')
        response.status = 400
        return 'User already exists!'
    try:
        rsa.PublicKey.load_pkcs1(publicKey)
    except ValueError:
        print('Incorrect key!')
        print(publicKey)
        response.status = 400
        return 'Incorrect public key!'
    userKeyFile = open(os.path.join('userkeys', userLogin + '.pem'), 'wb')
    userKeyFile.write(publicKey)
    userKeyFile.close()
    return 'OK\n' + getSelfPublicKey().save_pkcs1('PEM').decode('ascii')

@post('/setcheckin')
def setCheckIn():
    global points
    try:
        lat, long = map(float, recieveMessage().decode('ascii').split())
    except:
        response.status = 400
        return 'Invalid coords format!'
    if request.json['nick'] != 'admin':
        response.status = 403
        return 'Forbidden action!'
    points.append(Point(lat, long))
    points[-1].exportToDB(conn.cursor())
    conn.commit()
    print(points)

@post('/test')
def recieveMessage():
    try:
        request.json
    except:
        response.status = 400
        return 'Invalid JSON!'
    if type(request.json) != type(dict()):
        response.status = 400
        return 'JSON outer type must be object!'
    if 'nick' not in request.json:
        response.status = 400
        return 'Nick is not present!'
    if 'signature' not in request.json:
        response.status = 400
        return 'Signature is not present!'
    if 'message' not in request.json:
        response.status = 400
        return 'Message is not present!'

    nick, signature, message = request.json['nick'], request.json['signature'], request.json['message']
    try:
        decrypted = decryptMessage(nick, message.encode('ascii'), signature.encode('ascii'))
    except rsa.pkcs1.DecryptionError:
        response.status = 400
        return 'Can\'t decrypt your message!'
    except rsa.pkcs1.VerificationError:
        response.status = 400
        return 'Wrong signature!'
    return decrypted

#@get('/points')
#def sendPointsJSON():
#    global points
#    pointsJSON = [eval(p.exportToJSON()) for p in points]
#    return str(pointsJSON).replace("'", '"')

@get('/upoints')
def unsecureSendPoints():
    global points, conn
    if 'nick' not in request.params:
        response.status = 400
        return 'Give your nick.'
    team = playerTeam(conn.cursor(), request.params['nick'])
    if team is None:
        response.status = 400
        return 'You\'re not a player.'
    teamPoints = getPointsByTeam(conn.cursor(), team)
    for point in teamPoints:
        minDist = 10000000
        found = False
        for other in points:
            if point.belongs != other.belongs and (point.distToPoint(other) < minDist or not found):
                minDist = point.distToPoint(other)
                found = True
        point.minDist = (minDist if found else 0)
    pointsJSON = [eval(p.exportToJSON()) for p in teamPoints]
    return str(pointsJSON).replace("'", '"')

@get('/users')
def sendUsersJSON():
    global conn
    result = []
    c = conn.cursor()
    c.execute('SELECT `name`, `team` FROM Users')
    for row in c.fetchall():
        result += ['{{"name": "{}", "team": "{}"}}'.format(row[0], row[1])]
    return '[' + ', '.join(result) + ']'

@post('/game')
def joinGame():
    global teams, conn
    team = recieveMessage().decode('ascii').strip()
    if team not in teams:
        response.status = 400
        return 'Unexisting team: ' + team + '.'
    fullTeams = getFullTeams(conn.cursor())
    if len(fullTeams[team]) > min([len(fullTeams[k]) for k in fullTeams]):
        team = min(fullTeams, key=lambda k: len(fullTeams[k]))
    addPlayer(conn.cursor(), request.json['nick'], team)
    conn.commit()

@post('/ugame')
def unsecureJoinGame():
    global teams, conn
    try:
        request.json
    except:
        response.status = 400
        return 'Invalid JSON!'
    if type(request.json) != type(dict()):
        response.status = 400
        return 'JSON outer type must be object!'
    if 'nick' not in request.json:
        response.status = 400
        return 'Nick is not present!'
    if 'message' not in request.json:
        response.status = 400
        return 'Message is not present!'
    team = request.json['message'].strip()
    if team not in teams:
        response.status = 400
        return 'Unexisting team: ' + team + '.'
    fullTeams = getFullTeams(conn.cursor())
    if len(fullTeams[team]) > min([len(fullTeams[k]) for k in fullTeams]):
        team = min(fullTeams, key=lambda k: len(fullTeams[k]))
    addPlayer(conn.cursor(), request.json['nick'], team)
    conn.commit()

@post('/ucheckin')
def unsecureCheckIn():
    global points
    try:
        request.json
    except:
        response.status = 400
        return 'Invalid JSON!'
    if type(request.json) != type(dict()):
        response.status = 400
        return 'JSON outer type must be object!'
    if 'nick' not in request.json:
        response.status = 400
        return 'Nick is not present!'
    if 'message' not in request.json:
        response.status = 400
        return 'Message is not present!'
    try:
        lat, long = map(float, request.json['message'].split())
    except:
        response.status = 400
        return 'Invalid coords format!'
    if playerTeam(conn.cursor(), request.json['nick']) is None:
        response.status = 400
        return 'You\'re not a player!'
    for point in points:
        if point.isPointNear(Point(lat, long)):
            point.checkIn(playerTeam(conn.cursor(), request.json['nick']))
            point.exportToDB(conn.cursor())
    conn.commit()

@post('/checkin')
def checkIn():
    global points
    try:
        lat, long = map(float, receiveMessage().split())
    except:
        response.status = 400
        return 'Invalid coords format!'
    if playerTeam(conn.cursor(), request.json['nick']) is None:
        response.status = 400
        return 'You\'re not a player!'
    for point in points:
        if point.isPointNear(Point(lat, long)):
            point.checkIn(playerTeam(conn.cursor(), request.json['nick']))
            point.exportToDB(conn.cursor())
    conn.commit()

@get('/admin')
def displayAdminPage():
    global points
    templateFile = open('html/admin.html', 'rb')
    template = templateFile.read().decode('utf-8')
    templateFile.close()
    pointsList = ''
    for point in points:
        pointsList += '<li>' + str(point).replace('<', '&lt;').replace('>', '&gt;') + ' (<a href="/admindelcheckin?id=' + str(point.id) + '">Удалить</a>)</li>'
    template = template.replace('{{pointsList}}', pointsList)
    return template

@get('/admindelcheckin')
def adminDeleteCheckin():
    global conn, points
    if 'id' not in request.params:
        redirect('/admin')
    else:
        for i in range(len(points)):
            if str(points[i].id) == request.params['id']:
                del points[i]
                break
        c = conn.cursor()
        c.execute('DELETE FROM Points WHERE `id`=?', (request.params['id'],))
        conn.commit()
        redirect('/admin')

@post('/adminsetcheckin')
def adminAddCheckin():
    global points, conn
    add = True
    for p in ['lat', 'long', 'weight', 'eps', 'belongs']:
        if p not in request.forms:
            redirect('/admin', 303)
            add = False
    if add:
        point = Point(float(request.forms['lat']), float(request.forms['long']), float(request.forms['eps']))
        point.weight = float(request.forms['weight'])
        point.belongs = request.forms['belongs']
        point.exportToDB(conn.cursor())
        conn.commit()
        points += [point]
        redirect('/admin', 303)

@post('/mobilelogin')
def setNicknameCookie():
    if 'nick' not in request.forms:
        redirect('/mobile', 303)
    else:
        response.set_cookie('nick', request.forms['nick'])
        redirect('/mobile', 303)

@get('/mobile')
def getMobileVersion():
    if request.get_cookie('nick') is None:
        templateFile = open('html/login.html', 'rb')
        template = templateFile.read().decode('utf-8')
        templateFile.close()
    else:
        templateFile = open('html/mobile.html', 'rb')
        template = templateFile.read().decode('utf-8')
        templateFile.close()
    return template

@get('/doc')
def getDocs():
    templateFile = open('html/doc.txt', 'rb')
    template = templateFile.read().decode('utf-8').replace('\n', '<br>')
    templateFile.close()
    return template

@get('/smiles')
def getSmiles():
    templateFile = open('html/smile.txt', 'rb')
    template = '<tt style="display:block">' + templateFile.read().decode('utf-8').replace('<', '&lt;').replace('>', '&gt;').replace(' ', '&nbsp;').replace('\n', '<br>') + '</tt>'
    templateFile.close()
    return template

def main():
    global conn, teams
    if not os.path.isfile(os.path.join('mykeys', 'public.pem')) or not os.path.isfile(os.path.join('mykeys', 'private.pem')):
        init()
    conn = sqlite3.connect('points.db')
    loadFromDB(conn.cursor())
    teamsFile = open('teams')
    teams = teamsFile.read().splitlines()
    teamsFile.close()
    threading.Timer(6 * 60 * 60, increaseAllPoints) # 6 hours (6 hrs * 60 min in hour * 60 seconds in minute)
    run(host='0.0.0.0', port=1337)
    conn.close()

if __name__ == '__main__':
    main()
