from math import cos, acos, sin


class Point:
    def __init__(self, lat, long, eps=5):
        self.lat = lat
        self.long = long
        self.eps = eps
        self.belongs = 'nobody'
        self.weight = 100
        self.id = -1
        
    def checkIn(self, team):
        self.belongs = team
        self.weight /= 1.5

    def distToPoint(self, other):
        a = 90 - self.lat
        b = 90 - other.lat
        C = abs(self.long - other.long)
        dist = acos(cos(a) * cos(b) + sin(a) * sin(b) * cos(C)) * 6400
        return dist
    
    def isPointNear(self, other):
        return self.distToPoint(other) < self.eps

    def exportToJSON(self):
        result =  '{{"lat": {}, "long": {}, "eps": {}, "belongs": "{}", "weight": {}}}'.format(self.lat, self.long, self.eps, self.belongs, self.weight)
        if hasattr(self, 'minDist'):
            result = result[:-1] + ', "minDist": {}}}'.format(self.minDist)
        return result

    def exportToDB(self, cursor):
        if self.id == -1:
            cursor.execute('INSERT INTO Points VALUES (NULL, ?, ?, ?, ?, ?)', (self.lat, self.long, self.weight, self.eps, self.belongs))
            self.id = cursor.lastrowid
        else:
            cursor.execute('UPDATE Points SET `lat`=?, `long`=?, `weight`=?, `eps`=?, `belongs`=? WHERE `id`=?',
                           (self.lat, self.long, self.weight, self.eps, self.belongs, self.id))

    def __str__(self):
        return '<Point (id ' + str(self.id) + ') at ' + str(self.lat) + ',' + str(self.long) + ' with epsilon ' + str(self.eps) + ' and weight ' + str(self.weight) + ' belongs to ' + self.belongs + '>'

    def __repr__(self):
        return str(self)
